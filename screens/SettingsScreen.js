// // App.js
import React, {Component} from 'react'
import {Text, View} from 'react-native'

const RootContext = React.createContext()

const Grandson = () => (
  <RootContext.Consumer>
    {context=> {
      console.log('render')
      return (
        <View>
          <Text>{context.name}</Text>
          <Text onPress={() => alert(777)}>{context.name}</Text>
          <Text onPress={context.callName}>{context.name}</Text>
        </View>
      )
    }}
  </RootContext.Consumer>
)

const Son = () => (
  <Grandson />
)

export default class Father extends Component {
  state={
    name: 'kaba',
    age: 10,
    callName: () => console.warn('Hi kabaya')
  }
  render() {
    return (
      <View>
        <Text onPress={() => this.setState({age: 16})}>{this.state.age}</Text>
        <RootContext.Provider value={this.state}>
          <Son />
        </RootContext.Provider>
      </View>
    )
  }
}
