// @flow
import React from 'react';
import {View, Text, AsyncStorage} from 'react-native'
import { Provider, Subscribe, Container } from 'unstated';
import { PersistContainer } from 'unstated-persist'

export class CounterContainer extends PersistContainer {
  persist = {
    key: 'counter',
    version: 1,
    storage: AsyncStorage,
  }

  state = {
    count: 0,
    countHistory: []
  }

  increment() {
    this.setState({ count: this.state.count + 1, countHistory: [...this.state.countHistory, {lol:77}] });
  }

  decrement() {
    this.setState({ count: this.state.count - 1 });
  }
}

export class Counter extends React.Component {
  render() {
    return (
      <Subscribe to={[CounterContainer]}>
        {counter => (
          <View>
            <Text onPress={() => counter.decrement()}>-</Text>
            <Text>{counter.state.count}</Text>
            <Text onPress={() => counter.increment()}>+</Text>
            {counter.state.countHistory.map(x => <Text>{x.lol}</Text>)}
          </View>
        )}
      </Subscribe>
    )
  }
}

export default App = () => (
  <Provider>
    <Counter />
  </Provider>
)
