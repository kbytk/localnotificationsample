// local notificationのコピー
import React from 'react';
import { View, Text, Button, AsyncStorage} from 'react-native'
import { Notifications, Permissions } from 'expo'
import { Provider, Subscribe, Container } from 'unstated';
import { PersistContainer } from 'unstated-persist'

class LocalNotificationState extends PersistContainer {
  persist = {
    key: 'localNotificationState',
    version: 1,
    storage: AsyncStorage,
  }

  state = {
    tvPrograms: null
  }

  increment() {
    this.setState({ count: this.state.count + 1, countHistory: [...this.state.countHistory, {lol:77}] });
  }

  presentLocalNotification = () => {
    Notifications.presentLocalNotificationAsync(localNotification)
    const obj = {notificationId: j}
    console.warn('presentLocalNotification')
  }

  deleteAllScheduledNotification = () => {
    Notifications.dismissAllNotificationsAsync()
    console.warn('delete all reserved notification')
  }

  registerNotification = async(time) => {
    const notificationId = await Notifications.scheduleLocalNotificationAsync(scheduledNotification, {time})
  }

}

// ユーザーが時間を変更したとき、
// 1. 登録されている全てのlocalNotificationをdelete
// 2. asyncStorageから、scheduledNotificationInDbを呼び出す。
// 3. originalTimeOfNotificationにユーザーが、ユーザーが指定した時間を足して、購読させる。
// 4. scheduledNotificationInDbに変更を加え、dbに保存する。

// テレビ番組が終わる時のlocalNotificationの購読解除。
// テレビ番組が終わったら「興味があり」に分類されたListから削除されるので、考えなくていい。
// notificationが届くたびに、全てのlocalNotificationを削除して、また新しくlocalNotificationを作ればいい。

// storeのデータ構造の例(興味ありを押したアニメ番組表。)
const scheduledNotificationInDb = [
  {notificationKey: 'adsfa', originalTimeOfNotification: "2019-02-12T09:21:42.666Z"},
  {notificationKey: 'adsfa', originalTimeOfNotification: "2019-02-12T09:21:42.666Z"},
  {notificationKey: 'adsfa', originalTimeOfNotification: "2019-02-12T09:21:42.666Z"},
]

const AnimeA = {title: '興味ありと分類された何か', s: "2019-02-12T09:21:42.666Z"}

const localNotification = {
  title: 'タイトルです',
  body: '本文です', // (string) — body text of the notification.
  ios: { // (optional) (object) — notification configuration specific to iOS.
    sound: true // (optional) (boolean) — if true, play a sound. Default: false.
  },
  android: // (optional) (object) — notification configuration specific to Android.
  {
    sound: true, // (optional) (boolean) — if true, play a sound. Default: false.
    //icon (optional) (string) — URL of icon to display in notification drawer.
    //color (optional) (string) — color of the notification icon in notification drawer.
    priority: 'high', // (optional) (min | low | high | max) — android may present notifications according to the priority, for example a high priority notification will likely to be shown as a heads-up notification.
    sticky: false, // (optional) (boolean) — if true, the notification will be sticky and not dismissable by user. The notification must be programmatically dismissed. Default: false.
    vibrate: true // (optional) (boolean or array) — if true, vibrate the device. An array can be supplied to specify the vibration pattern, e.g. - [ 0, 500 ].
    // link (optional) (string) — external link to open when notification is selected.
  }
}

const scheduledNotification = {
  title: '１０秒後に送られました。',
  body: '本文です', // (string) — body text of the notification.
  ios: { // (optional) (object) — notification configuration specific to iOS.
    sound: true // (optional) (boolean) — if true, play a sound. Default: false.
  },
  android: // (optional) (object) — notification configuration specific to Android.
  {
    sound: true, // (optional) (boolean) — if true, play a sound. Default: false.
    //icon (optional) (string) — URL of icon to display in notification drawer.
    //color (optional) (string) — color of the notification icon in notification drawer.
    priority: 'high', // (optional) (min | low | high | max) — android may present notifications according to the priority, for example a high priority notification will likely to be shown as a heads-up notification.
    sticky: false, // (optional) (boolean) — if true, the notification will be sticky and not dismissable by user. The notification must be programmatically dismissed. Default: false.
    vibrate: true // (optional) (boolean or array) — if true, vibrate the device. An array can be supplied to specify the vibration pattern, e.g. - [ 0, 500 ].
    // link (optional) (string) — external link to open when notification is selected.
  }
}

let time = new Date()
time.setSeconds(time.getSeconds() + 1000)
const schedulingOptions = {
  time: time // (date or number) — A Date object representing when to fire the <no></no>tification or a number in Unix epoch time. Example: (new Date()).getTime() + 1000 is one second from now.
}

class ScheduleSettingsScreen extends React.Component {
  componentDidMount = async () => {
    let result = await
      Permissions.askAsync(Permissions.NOTIFICATIONS);
    if (Constants.lisDevice && result.status === 'granted') {
      console.log('Notification permissions granted.')
    }
  }
  
  scheduleLocalNotification = async() => {
    const id = await Notifications.scheduleLocalNotificationAsync(scheduledNotification, schedulingOptions)
    console.log(id)
  }

  subscribeLocalNotification = async() => {
    const notificationId = await Notifications.scheduleLocalNotificationAsync(scheduledNotification, schedulingOptions)
  }


  render() {
    console.warn('rendered')
    console.log('Notifications')
    console.log(Notifications)
    const {unstated} = this.props
    console.warn(unstated)
    let time = new Date()
    time.setSeconds(time.getSeconds() + 8000)
    
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home!</Text>
        <Button onPress={() => unstated.presentLocalNotification()} title='すぐにプッシュ通知を送る。' />
        <Button onPress={() => this.scheduleLocalNotification()} title='１０秒後にプッシュ通知を送る。' />
        <Button onPress={() => unstated.deleteAllScheduledNotification()} title='全てのイベントを削除。' />
        <Button onPress={() => unstated.registerNotification(time)} title='アニメAをプッシュ通知として登録' />
        <Button onPress={() => this.scheduleLocalNotification()} title='アニメAだけプッシュ通知を削除' />
      </View>
    )
  }
}

const WrapperClass = () => (
  <Subscribe to={[LocalNotificationState]}>
    {unstated => <ScheduleSettingsScreen unstated={unstated} />}
  </Subscribe>
)


export default App = () => (
  <Provider>
    <WrapperClass />
  </Provider>
)