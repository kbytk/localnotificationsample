import 'react-native';
import React from 'react';
import App from '../App';
import renderer from 'react-test-renderer';
import NavigationTestUtils from 'react-navigation/NavigationTestUtils';

describe('App snapshot', () => {
  jest.useFakeTimers();
  beforeEach(() => {
    NavigationTestUtils.resetInternalState();
  });

  it('renders the loading screen', async () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders the root without loading screen', async () => {
    const tree = renderer.create(<App skipLoadingScreen />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

// 15ふん学習した。

import {CounterContainer, Counter} from '../screens/HomeScreen'
import {Provider} from 'unstated'


// このように超絶シンプルにテストを書ける。
it('counter', async () => {
  let counter = new CounterContainer()
  expect(counter.state.count).toBe(0)

  await counter.increment()
  expect(counter.state.count).toBe(1)

  await counter.decrement()
  expect(counter.state.count).toBe(0)
});

// it('counter tree', async () => {
//   let counter = new CounterContainer();
//   let tree = (
//     <Provider inject={[counter]}>
//       <Counter />
//     </Provider>
//   )

// jestでそのままスナップショットができる。
it('renders the Home screen', async () => {
  const tree = renderer.create(
    <Provider>
      <Counter />
    </Provider>
  )
  expect(tree.toJSON()).toMatchSnapshot();
})

it('renders incremented the Home screen', async () => {
  const tree = renderer.create(
    <Provider>
      <Counter />
    </Provider>
  ).root
  testInstance = tree.root

  debugger
  expect(tree.toJSON()).toMatchSnapshot()
})
  
  // debugger
  // await tree.insatance().decrement()
  // expect(counter.state.count).toBe(1);

  // await tree.decrement()
  // expect(counter.state.count).toBe(0);
// });